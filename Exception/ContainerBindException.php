<?php

/**
 * This File is part of Stream\IoC package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\IoC\Exception;
/**
 * Class ContainerBindException
 */
class ContainerBindException extends \Exception {}

