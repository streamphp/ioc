<?php

/**
 * This File is part of the Stream\IoC\Loaders package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\IoC\Loaders;

use \DOMDocument;
use Stream\Serializer\SimpleXMLElement;

/**
 * Class: XmlLoader
 *
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class XmlLoader
{
    /**
     * load
     *
     * @param mixed $resource
     * @access public
     * @return mixed
     */
    public function load($resource)
    {
        $dom = new DOMDocument;
        $dom->load(realpath($resource), LIBXML_NONET | defined('LIBXML_COMPACT') ? LIBXML_COMPACT : 0);
        $xml = simplexml_import_dom($dom, 'Stream\\Serializer\\SimpleXMLElement');

        $this->parseContainer($xml);
    }

    /**
     * parseContainer
     *
     * @param SimpleXMLElement $xml
     * @access protected
     * @return mixed
     */
    protected function parseContainer(SimpleXMLElement $xml)
    {
        var_dump($xml->container);
        $containers = $xml->xpath('//*/container');
        var_dump($containers);
    }
}
