<?php

/**
 * This File is part of the Stream\IoC package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */
namespace Stream\IoC;

/**
 * @class Definition
 */

class Definition
{
    /**
     * __construct
     *
     * @access public
     * @return mixed
     */
    public function __construct()
    {

    }

    public function name($param)
    {
        return null;
    }
}
