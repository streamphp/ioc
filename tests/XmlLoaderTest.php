<?php

/**
 * This File is part of the www\stream\src\Stream\IoC\tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\IoC;

use Stream\IoC\Loaders\XmlLoader;

/**
 * Class: XmlLoaderTest
 *
 * @uses \PHPUnit_Framework_TestCase
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class XmlLoaderTest extends \PHPUnit_Framework_TestCase
{
    public function testLoadXMLFile()
    {
        $file = dirname(__DIR__) . '/Schema/example.xml';
        $loader = new XmlLoader();
        $loader->load($file);
    }

}
