<?php

/**
 * This File is part of the Stream\IoC package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\IoC;

use Stream\IoC\Container as Container;
use Stream\IoC\Exception\ContainerResolutionException;
use Stream\IoC\Exception\ContainerBindException;
use \Mockery as m;

/**
 * Class    ContainerTest
 *
 * @uses    \PHPUnit_Framework_TestCase
 *
 * @package
 * @version
 * @author  Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class ContainerTest extends \PHPUnit_Framework_TestCase
{
    protected $container;

    protected function setUp()
    {
        $this->container = new Container();
    }

    protected function tearDown()
    {
        m::close();
    }

    /**
     * @test
     * @covers Container#bind()
     * @covers Container#resolve()
     */
    public function testBindClosureToContainer()
    {
        $this->container->bind('test', function () {
            return 'foo';
        });

        $this->assertEquals('foo', $this->container->resolve('test'));
    }

    /**
     * @test
     */
    public function testAddSettersAsAnonnymousClass()
    {
        $this->getMock('Stream\Tests\IoC\ClassA');

        $this->container
            ->bind('Stream\Tests\IoC\ContainerTestClassWithSetter')
            ->addSetter('setObject', 'Stream\Tests\IoC\ClassA');
        $class = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassWithSetter');

        $this->assertInstanceOf('Stream\Tests\IoC\ClassA', $class->getObject());
    }

    /**
     * @test
     */
    public function testAddSettersAsClosure()
    {
        $this->container['foo'] = $this->container->share(function () {
            return new ClassA;
        });

        $this->container
            ->bind('Stream\Tests\IoC\ContainerTestClassWithSetter')
            ->addSetter('setObject', 'foo');
        $class = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassWithSetter');

        $this->assertInstanceOf('Stream\Tests\IoC\ClassA', $class->getObject());
        $this->assertSame($this->container['foo'], $class->getObject());
    }


    /**
     * @test
     */
    public function testAddSettersOnSharedObject()
    {
        $this->container['foo'] = $this->container->share(function () {
            return new ClassA;
        });

        $this->container['service.foo'] = $this->container->share(function () {
            return new ContainerTestClassWithSetter;
        });
        $this->container
            ->addSetter('setObject', 'foo');
        $class = $this->container->resolve('service.foo');

        $this->assertInstanceOf('Stream\Tests\IoC\ClassA', $class->getObject());
        $this->assertSame($this->container['foo'], $class->getObject());
    }

    /**
     * @test
     */
    public function testAddSettersOnSingleton()
    {
        $this->container['foo'] = $this->container->share(function () {
            return new ClassA;
        });

        $this->container
            ->bindSingleton('Stream\Tests\IoC\ContainerTestClassWithSetter')
            ->addSetter('setObject', 'foo');
        $class = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassWithSetter');

        $this->assertInstanceOf('Stream\Tests\IoC\ClassA', $class->getObject());
        $this->assertSame($this->container['foo'], $class->getObject());
    }

    /**
     * @test
     */
    public function testClassAliasBind()
    {
        $this->container['foo'] = 'Stream\Tests\IoC\ContainerTestClassWithSetter';
        $this->container->bind('bar', 'Stream\Tests\IoC\ContainerTestClassWithSetter');

        $array = [
            'foo' => 'bar'
        ];

        $this->assertInstanceOf('Stream\Tests\IoC\ContainerTestClassWithSetter', $this->container['foo']);
        $this->assertInstanceOf('Stream\Tests\IoC\ContainerTestClassWithSetter', $this->container['bar']);
    }

    /**
     * @test
     * @covers Container#bind()
     * @covers Container#resolve()
     */
    public function testResolveClosureWithArgs()
    {
        $this->getMock('ClassA');

        $this->container->bind('test', function ($a, $b) {
            return $a + $b;
        });

        $this->container->bind('foo', function ($a = 2, $b = 3) {
            return $a * $b;
        });

        $this->container->bind('baz', function (array &$arr = [2, 4]) {
            return $arr[0] * $arr[1];
        });

        $this->assertEquals(5, $this->container->resolve('test', array(2, 3)));
        $this->assertEquals(6, $this->container->resolve('foo'));
        $this->assertEquals(8, $this->container->resolve('baz'));
    }

    public function testAddArguments()
    {
        $this->getMock('ClassA');
        $this->container
            ->bind('service.foobar', 'Stream\Tests\IoC\ContainerTestClassB')
            ->addArgument('foo');

        $this->container['foo'] = $this->container->share(function () {
            return new ClassA;
        });

        $instanceA = $this->container->resolve('service.foobar');
        $instanceB = $this->container->resolve('service.foobar');
        $this->assertFalse($instanceA === $instanceB);
        $this->assertInstanceOf('Stream\Tests\IoC\ClassA', $instanceA->getObject());
        $this->assertTrue($instanceA->getObject() === $instanceB->getObject());

        $this->container
            ->bind('Stream\Tests\IoC\ContainerTestClassA')
            ->addArgument(['a', 'b']);
        $instance = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassA');
        $this->assertEquals(['a', 'b'], $instance->bar);

    }

    public function testAddArgumentWithClosure()
    {
        $this->container
            ->bind('foo', ['a', 'b']);
        $this->container
            ->bind('Stream\Tests\IoC\ContainerTestClassA')
            ->addArgument(function ($container) {
                return $container->resolve('foo');
            });
        $instance = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassA');
        $this->assertEquals(['a', 'b'], $instance->bar);
    }


    /**
     * @test
     * @covers Container#resolve()
     */
    public function testResolveNoneBoundClass()
    {
        $class = $this->getMock('Stream\Tests\IoC\ClassA');
        $object = $this->container->resolve('Stream\Tests\IoC\ClassA');

        $this->assertTrue($object instanceof ClassA);
    }

    /**
     * @test
     * @covers Container#bind()
     * @covers Container#resolve()
     */
    public function testBindInterfaceTOImplementation()
    {
        $this->container->bind('Stream\Tests\IoC\TestInterfaceA', 'Stream\Tests\IoC\ContainerTestClassA');
        $object = $this->container->resolve('Stream\Tests\IoC\TestInterfaceA');

        $this->assertTrue($object instanceof TestInterfaceA);
        $this->assertInstanceOf('Stream\Tests\IoC\ContainerTestClassA', $object);

        $object = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassD');
        $this->assertTrue($object->getObject() instanceof TestInterfaceA);
    }

    /**
     * @test
     * @covers Container#resolve()
     */
    public function testResolveClassWithParams()
    {
        $class = $this->getMock('Stream\Tests\IoC\ClassA');
        $object = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassB');
        $this->assertInstanceOf('Stream\Tests\IoC\ContainerTestClassB', $object);
        $this->assertTrue($object->getObject() instanceof ClassA);
    }

    /**
     * @test
     * @covers Container#resolve()
     */
    public function testResolvedClassesHaveDifferentInstances()
    {
        $objectA = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassB');
        $objectB = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassB');

        $this->assertFalse($objectA === $objectB);
    }

    /**
     * @test
     */
    public function arrayAcccessBindCallback()
    {
        $this->container['foo'] = function () {
            return 'foo';
        };
        $this->assertEquals('foo', $this->container->resolve('foo'));
    }

    /**
     * @test
     */
    public function arrayAcccessBindSharedObject()
    {
        $mock = m::mock('Bar');

        $this->container['foo'] = $this->container->share(function () use ($mock) {
            return $mock;
        });

        $foo = $this->container->resolve('foo');
        $bar = $this->container->resolve('foo');
        $this->assertSame($foo, $bar);
    }

    /**
     * @test
     */
    public function testProtectedClosure()
    {
        $callable = function () {
            return 'foo';
        };

        $this->container['param'] = $this->container->parameter($callable);
        $this->assertSame($callable, $this->container['param']);
        $this->assertSame('foo', $this->container['param']());
    }

    /**
     * @test
     * @covers Container#share()
     * @covers Container#resolve()
     */
    public function testResolveSingleton()
    {
        $class = $this->getMock('ClassA');

        $this->container->bindSingleton('Stream\Tests\IoC\ContainerTestClassC');

        $objectA = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassC');
        $objectB = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassC');
        $objectC = $this->container->resolve('Stream\Tests\IoC\ContainerTestClassC');

        $this->assertInstanceOf('Stream\Tests\IoC\ContainerTestClassC', $objectA);
        $this->assertTrue($objectC === $objectA);
    }

    /**
     * @test
     * @expectedException Stream\IoC\Exception\ContainerResolutionException
     */
    public function testUnresolvable()
    {
        $object = $this->container->bind('foo', function ($a) {
            return $a;
        });
        $object = $this->container->resolve('foo');
    }

    public function testInject()
    {
        $this->container->bind('foo', function () {
            return new ContainerTestClassA();
        });

        $this->container->inject('foo', function ($class) {
            $class->name = 'bar';
            return $class;
        });
        $object = $this->container['foo'];

        $this->assertInstanceOf('Stream\Tests\IoC\ContainerTestClassA', $object);
        $this->assertEquals('bar', $object->name);
    }

    /**
     * @test
     * @expectedException Stream\IoC\Exception\ContainerResolutionException
     */
    public function testResolveFailsBecauseOfPrivateConstrutor()
    {
        $object = $this->container->bind('Stream\Tests\IoC\ContainerTestSingleton');
        $object = $this->container->resolve('Stream\Tests\IoC\ContainerTestSingleton');
    }

    /**
     * @test
     * @expectedException Stream\IoC\Exception\ContainerBindException
     */
    public function testBindClassToExistingClassFails()
    {
        $this->container->bind('Stream\Tests\IoC\ContainerTestClassA', 'Stream\Tests\IoC\ContainerTestClassB');
    }
    /**
     * Don't allow a class to be bound on a foreign interface
     *
     * @test
     * @expectedException Stream\IoC\Exception\ContainerBindException
     */
    public function testBindImplementationToWrongInterface()
    {
        $object = $this->container->bind('Stream\Tests\IoC\TestInterfaceA', 'Stream\Tests\IoC\ContainerTestClassB');
    }
}

interface TestInterfaceA
{
    public function foo();
}

class ContainerTestClassA implements TestInterfaceA
{
    public function __construct(array $foo = null)
    {
        $this->bar = $foo;
    }
    public function foo()
    {
    }
}

class ContainerTestSingleton
{
    private function __construct()
    {

    }
}
class ContainerTestClassB
{
    protected $object;

    public function __construct(ClassA &$object = null)
    {
        $this->object = $object;
    }


    public function getObject()
    {
        return $this->object;
    }
}

class ContainerTestClassWithSetter extends ContainerTestClassB
{
    public function __construct()
    {
    }

    public function setObject(ClassA &$object)
    {
        $this->object = $object;
    }
}

class ContainerTestClassC
{
    protected $object;

    public function __construct(ContainerTestClassB &$object)
    {
        $this->object = $object;
    }

    public function getObject()
    {
        return $this->object;
    }
}

class ContainerTestClassD
{
    protected $object;

    public function __construct(TestInterfaceA $object)
    {
        $this->object = $object;
    }

    public function getObject()
    {
        return $this->object;
    }
}
