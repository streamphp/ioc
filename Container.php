<?php

/**
 * This File is part of the Stream\IoC package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */
namespace Stream\IoC;

use Closure;
use ReflectionClass;
use ReflectionParameter;
use ReflectionFunction;

use Stream\IoC\Exception\ContainerResolutionException;
use Stream\IoC\Exception\ContainerBindException;

/**
 * Container
 *
 * @package Stream\Ico
 *
 * @version GIT: $Id$ in Development
 *
 * @copyright 2012-2015 Thomas Appel. <http://thomas-appel.com>
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class Container implements InterfaceContainer
{
    /**
     * Storage of bound blueprints or objects
     *
     * @var array
     */
    protected $bindings  = [];

    /**
     * setters
     *
     * @var array
     */
    protected $setters  = [];

    /**
     * arguments
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * holds a reference of a resolved object that was registered as »shared«
     *
     * @var array
     */
    protected $sharedObjects = [];

    /**
     * reflectionStorage
     *
     * @var array
     */
    protected $reflectionStorage = [];

    /**
     * lastBinding
     *
     * @var string
     */
    protected $lastBinding;

    /**
     * array access offsetExists
     *
     * Determine if an id is bound to the container
     *
     * @param string $offset
     * @access public
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return $this->isBound($offset);
    }

    /**
     * array access offsetGet
     *
     * resolve a bound object;
     *
     * @param string $offset
     * @access public
     * @return mixed
     */
    public function offsetGet($offset)
    {
        if (!$this->isBound($offset)) {
            throw new \InvalidArgumentException(sprintf('indedfined index %s', (string)$offset));
        }
        return $this->resolve($offset);
    }

    /**
     * array access offsetSet
     *
     * @see InterfaceContainer#bind()
     *
     * @param string $offset
     * @param mixed $value
     * @access public
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->bind($offset, $value);
    }

    /**
     * array access offsetUnset
     *
     * Unset a bound definition
     *
     * @param string $offset
     * @access public
     * @return void
     */
    public function offsetUnset($offset)
    {
        if ($this->isBound($offset)) {

            if ($this->isShared($offset)) {
                unset($this->sharedObjects[$offset]);
            }

            unset($this->bindings[$offset]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function bind($identifier, $implementation = null, $shared = false)
    {
        if ($this->isBound($identifier)) {
            throw new ContainerBindException(sprintf("%s is already bound", $identifier));
        }

        if (is_null($implementation) && !class_exists($identifier)) {
            throw new ContainerBindException(sprintf("%s: can't bind an empty implementation to an alias", $identifier));
        }

        if (is_string($implementation)) {

            if (interface_exists($identifier)) {

                $reflection = $this->getReflectionClass($implementation);

                if (!$reflection->implementsInterface($identifier)) {
                    throw new ContainerBindException("$implementation does not implement $identifier");
                }

            } elseif ((class_exists($identifier) && class_exists($implementation))) {
                throw new ContainerBindException(sprintf("can't bind exsiting class %s to existing class %s", $implementation, $identifier));
            }
        }

        $this->bindings[$identifier] = $implementation;

        if ($shared) {
            $this->bindings[$identifier] = $this->bindings[$identifier] = $this->share(
                $this->bindSingletonConstruction($identifier, $implementation)
            );
        }
        $this->lastBinding = $identifier;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function resolve($identifier, $with = [])
    {

        if ($this->isShared($identifier)) {
            $instance = $this->sharedObjects[$identifier];

            if ($this->hasSetters($identifier)) {
                $this->applySetters($identifier, $instance);
            }

            return $instance;
        }

        $class = false;

        if ($this->isBound($identifier)) {

            if ($this->bindings[$identifier] instanceof Closure) {
                $with = is_array($with) ? $with : (is_null($with) ? [] : [$with]);
                return $this->resolveFactory($this->bindings[$identifier], $identifier, $with);
            }

            if (!is_null($this->bindings[$identifier]) && !is_scalar($this->bindings[$identifier])) {
                return $this->bindings[$identifier];
            }

            $class = class_exists($identifier) ? $identifier : (class_exists($this->bindings[$identifier]) ?
                $this->bindings[$identifier] : false);

            if (false === $class) {
                return $this->bindings[$identifier];
            }

        } else {
            $class = (true === class_exists($identifier)) ? $identifier : false;
        }

        if (false === $class) {
            throw new ContainerResolutionException(sprintf("class %s does not exist", $identifier));
        }

        $parameter = null;
        $reflection = $this->getReflectionClass($class);

        if (!$reflection->isInstantiable()) {
            throw new ContainerResolutionException("class $class can't be resolved because it's not instantiable");
        }

        if ($reflectionConstructor = $reflection->getConstructor()) {
            $parameter = $reflectionConstructor->getParameters();

            if (!count($parameter)) {
                $instance = new $class();

            } else {
                $ar = [];
                $arguments =& $ar;

                $resolvedParams = (array)$this->resolveParameter($parameter, $ar, $identifier);
                $instance = $reflection->newInstanceArgs($resolvedParams);
            }

        } else {
            $instance = $reflection->newInstanceWithoutConstructor();
        }

        if ($this->hasSetters($identifier)) {
            $this->applySetters($identifier, $instance);
        }

        return $instance;
    }

    /**
     * {@inheritDoc}
     */
    public function bindSingleton($identifier, $implementation = null)
    {
        return $this->bind($identifier, $implementation, true);
    }

    /**
     * {@inheritDoc}
     */
    public function share(Closure $callable)
    {
        return function () use ($callable) {

            static $object;

            if (is_null($object)) {
                $object = $callable($this);
            }

            return $object;
        };
    }

    /**
     * {@inheritDoc}
     */
    public function parameter(Closure $callable)
    {
        return function () use ($callable) {
            return $callable;
        };
    }

    /**
     * {@inheritDoc}
     */
    public function inject($identifier, Closure $callable)
    {
        if (!$this->isBound($identifier)) {
            throw new InvalidArgumentException(sprintf('%s is not defined', $identifier));
        }

        $constructor = $this->bindings[$identifier];

        if (!$constructor instanceof Closure) {
            throw new InvalidArgumentException(sprintf('%s contains no definition', $identifier));
        }

        $this->bindings[$identifier] = function ($arg = null) use ($identifier, $constructor, $callable) {
            return $callable($constructor($arg), $arg);
        };
        return $this;
    }


    /**
     * {@inheritDoc}
     */
    public function hasSetters($identifier)
    {
        return isset($this->setters[$identifier]);
    }

    /**
     * {@inheritDoc}
     */
    public function hasArguments($identifier)
    {
        return array_key_exists($identifier, $this->arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function addSetter($method, $reference, $binding = null)
    {
        $current = is_null($binding) ? $this->getLastBinding() : $this->getLastBinding();

        if (!$this->hasSetters($current)) {
            $this->setters[$current] = [];
        }

        $this->setters[$current][] = function ($instance) use ($method, $reference)
        {
            $reference = $this->resolveArgument($reference);
            try {
                $instance->{$method}($reference);
            } catch (\Exception $e) {
                throw new ContainerResolutionException($e->getMessage());
            }
        };
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function addArgument($argument)
    {
        $current = $this->getLastBinding();

        $this->arguments[$current][] = function () use ($argument)
        {
            $argument = $this->resolveArgument($argument);
            return $argument;
        };

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function keys()
    {
        return array_keys($this->bindings);
    }

    /**
     * check if there’s a binding with that identifier
     *
     * @param string $identifier
     */
    protected function isBound($identifier)
    {
        return array_key_exists($identifier, $this->bindings);
    }

    /**
     * check if there’s a shared object that's been registered with the
     * given identifier
     *
     * @param string $identifier
     */
    protected function isShared($identifier)
    {
        return isset($this->sharedObjects[$identifier]);
    }

    /**
     * bindSingletonConstruction
     *
     * @param mixed $identifier
     * @param mixed $implementation
     * @access protected
     * @return mixed
     */
    protected function bindSingletonConstruction($identifier, $implementation = null)
    {
        return function () use ($identifier, $implementation) {
            unset($this->bindings[$identifier]);
            $this->bind($identifier, $implementation);
            $this->sharedObjects[$identifier] = $this->resolve($identifier);
            return $this->sharedObjects[$identifier];
        };
    }

    /**
     * Resolve parameter retrieved from `RefelctionFunctionAbstract#getParameters()`
     *
     * @param array $parameter
     * @return stdClass
     */
    protected function resolveParameter(&$params, array &$resolved = [], $identifier = null)
    {
        $args = $this->getArguments($identifier, $resolved);

        foreach ($params as $index => $param) {
            if (isset($args[$index])) {
                $res = $args[$index];
            } elseif ($class = $param->getClass()) {
                $res = $this->resolve($class->getName());
            } else {
                $res = $this->resolveReflectionParam($param);
            }

            if ($param->isPassedByReference()) {
                $resolved[] = &$res;
            } else {
                $resolved[] = $res;
            }
        }
        return (object)$resolved;
    }

    /**
     * Resolve a reflection parameter
     *
     * @param ReflectionParameter $param
     * @throws ContainerResolutionException
     */
    protected function resolveReflectionParam(ReflectionParameter $param)
    {
        if ($param->isDefaultValueAvailable()) {
            return $param->getDefaultValue();
        }
        $name = $param->getName();
        throw new ContainerResolutionException("parameter $name cannot be resolved");
    }

    /**
     * Resolve a factory callback
     *
     * @param Closure $factory
     * @param array $arguments
     * @param string $identifier
     * @access protected
     * @return mixed
     */
    protected function resolveFactory(Closure $factory, $identifier, array &$arguments = [])
    {
        // workaround to get back arguments that where passed by reference
        // as actual arguments passed by reference:
        $argv = [];

        if (empty($arguments)) {
            $reflection = $this->getReflectionFunction($factory, 'reflection_factory_' . $identifier);

            if ($params = $reflection->getParameters()) {
                $arguments = (array)$this->resolveParameter($params, $argv, $identifier);
            }

        } else {
            $arguments = is_array($arguments) ? $arguments : [$arguments];
        }

        $args = &$arguments;

        $instance = call_user_func_array($factory, $args);

        if ($this->hasSetters($identifier)) {
            $this->applySetters($identifier, $instance);
        }

        return $instance;
    }


    /**
     * getLastBinding
     *
     * @access protected
     * @return mixed
     */
    protected function getLastBinding()
    {
        return $this->lastBinding;
    }

    /**
     * resolveArgument
     *
     * @param mixed $argument
     * @access protected
     * @return mixed
     */
    protected function resolveArgument($argument)
    {
        if ($argument instanceof Closure) {
            $argument = $argument($this);
        } elseif (is_string($argument) && (class_exists($argument) || $this->isBound($argument))) {
            $argument = $this->resolve($argument);
        }
        return $argument;
    }

    /**
     * applySetters
     *
     * @param mixed $identifier
     * @param mixed $instance
     * @access protected
     * @return void
     */
    protected function applySetters($identifier, &$instance)
    {
        foreach ($this->setters[$identifier] as $setter) {
            $setter($instance);
        }
    }

    /**
     * applyArguments
     *
     * @param mixed $identifier
     * @param mixed $instance
     * @access protected
     * @return array
     */
    protected function getArguments($identifier, &$args = [])
    {
        if ($this->hasArguments($identifier)) {
            foreach ($this->arguments[$identifier] as $argument) {
                $arg = $argument();
                $args[] =& $arg;
            }
        }
        return $args;
    }

    /**
     * getReflectionClass
     *
     * @param mixed $class
     * @access protected
     * @return ReflectionClass
     */
    protected function getReflectionClass($class)
    {
        if (!isset($this->reflectionStorage[$class])) {
            $this->reflectionStorage[$class] = new ReflectionClass($class);
        }
        return $this->reflectionStorage[$class];
    }

    /**
     * getReflectionFunction
     *
     * @param mixed $method
     * @access protected
     * @return ReflectionFunction
     */
    protected function getReflectionFunction(Closure $method, $id)
    {
        if (!isset($this->reflectionStorage[$id])) {
            $this->reflectionStorage[$id] = new ReflectionFunction($method);
        }
        return $this->reflectionStorage[$id];

    }
}
